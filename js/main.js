jQuery(document).ready(function($) {

	/* Autoselect everything on focus */
	document.querySelector('#pastearea').onfocus = function(e) {
		var el = this;
		requestAnimationFrame(function() {
			selectElementContents(el);
		});
	};

	function selectElementContents(el) {
		var range = document.createRange();
		range.selectNodeContents(el);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
	};

	/* Focus it so autoselect works */
	$('#pastearea').focus();
	$('input').on('focus', function() {
		this.select();
	});

	/* Name the file */
	$('#lets_do_it').on('click', function() {
		$nombre_de_archivo = $('input[name="nombre_de_archivo"]').val();

		/* Output type */
		$extension = $('select[name="extension"]').val();
		if ($extension == 'md') {
			$output_type = 0;
		} else {
			$output_type = 1;
		};

		if ($nombre_de_archivo != 'Optionally type here a name for your file') {
			$nombre = $nombre_de_archivo + '.' + $extension;
		} else {
			$nombre = 'Markdownified file' + '.' + $extension;
		};

		/* Extract all URLs from #pastearea and build an array with them */
		$contenido = $('#pastearea').html();

		$documento_temporal = document.createElement("html");
		$documento_temporal.innerHTML = $contenido;
		$links = $documento_temporal.getElementsByTagName("a");
		var direcciones = [];

		for (var i = 0; i < $links.length; i++) {
			direcciones.push($links[i].getAttribute("href"));
		};

		/* Cicle throug the array and query each url using heckyesmarkdown.com API */
		var i = 0;

		function repeticion_uno() {
			setTimeout(function() {

				$direccion = 'http://heckyesmarkdown.com/go/?read=1&preview=' + $output_type + '&showframe=0&u=' + direcciones[i];

				/* The query part */
				$.ajax({
					url: $direccion,
					type: 'GET'
				}).done(function(data) {

					/* Make the file and dowload the data */
					function download(filename, text) {

						var link = document.createElement('a');
						link.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
						link.setAttribute('download', filename);

						if (document.createEvent) {
							var event = document.createEvent('MouseEvents');
							event.initEvent('click', true, true);
							link.dispatchEvent(event);
						} else {
							link.click();
						};

					};

					download($nombre, data);
					console.log('Iteración ' + i + ' - ' + $direccion);
					alert(data);
				});

				i++;

				if (i < direcciones.length) {
					repeticion_uno();
				} else {
					if ($extension == 'html') {
						alert('Queue succesfully completed.\n\nThe output HTML is just a code snippet without the complete HTML structure. Edit it and add propper HTML headers if you want to read it in your browser, else you may see WEIRD CHARACTERS!!!\n\nYou have been warned.');
					} else {
						alert('Queue succesfully completed.');
					};
				};

			}, 3000);
		};

		repeticion_uno();

	});
});
