# Url-to-markdown-script
An easy to use script that runs on your browser and lets you quickly download many webpages converting them to markdown (using heckyesmarkdown's API).

## Instructions

 - Download or clone this repository.
 - Unzip it and 2-click on *"index.html"* file to run it on your default browser.